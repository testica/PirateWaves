#version 400

layout (location = 0) in vec3 VertexPosition;
in vec3 normal;
in vec2 texture;

uniform mat4 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

out vec3 fNormal;
out vec3 fPosition;
out vec2 text;


void main()
{
	text = texture;
	fNormal = normalize((normalMatrix * vec4(normal,0.0)).xyz);
	vec4 pos = modelViewMatrix * vec4(VertexPosition, 1.0);
	fPosition = pos.xyz;
	gl_Position = projectionMatrix * pos;
}