#version 400

in vec3 fNormal;
in vec3 fPosition;
in vec2 text;
uniform sampler2D image;
uniform vec3 l1Pos;
uniform vec3 l2Pos;	
uniform vec3 l1Amb;
uniform vec3 l1Diff;
uniform vec3 l1Spec;
uniform vec3 l2Amb;
uniform vec3 l2Diff;
uniform vec3 l2Spec;
uniform vec3 ka;
uniform vec3 kd;
uniform vec3 ks;
uniform float shine;
out vec4 FragColor;

void main()
{
	float d, spec, d2, spec2;
	vec3 eyePos = vec3(3.0, 3.0, 10.0);
	vec3 fNormal1 = normalize(fNormal);
	//float shine = 0.9;
	vec3 s = normalize(l1Pos - fPosition);
	vec3 diffuse, specular, r, v, diffuse2, specular2, r2, s2, h, h2;
	s2 = normalize(l2Pos - fPosition);
	float alfa = dot(s,fNormal1);
	float alfa2 = dot(s2, fNormal1);
	d = max(alfa, 0.0);
	d2 = max(alfa2, 0.0);
	diffuse = d * l1Diff;
	diffuse = diffuse * kd;
	diffuse2 = d2 * l2Diff;
	diffuse2 = diffuse2 * kd;
	//PHONG
	//r = normalize(reflect(-s, fNormal1));
	//r2 = normalize(reflect(-s2, fNormal1));
	v = normalize(eyePos - fPosition);
	//BLINN
	h = normalize(s + v);
	h2 = normalize(s2 + v);
	spec = dot(h, fNormal1);
	spec2 = dot(h2, fNormal1);
	spec = max(spec, 0.0);
	spec2 = max(spec2, 0.0);
	spec = pow(spec,shine);
	spec2 = pow(spec2, shine);
	specular = spec * (l1Spec*ks);
	specular2 = spec2 * (l2Spec*ks);
	//FragColor = vec4(specular + specular2 + diffuse + diffuse2 + (l1Amb*ka) + (l2Amb*ka), 1.0) * texture(image, text);
	FragColor = texture2D(image, text);
}