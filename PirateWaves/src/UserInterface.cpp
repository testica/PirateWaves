#include "UserInterface.h"
#include "Main.h"
extern int gWidth, gHeight;

// Global static pointer used to ensure a single instance of the class.
CUserInterface * CUserInterface::mInterface = NULL;

/**
* Creates an instance of the class
*
* @return the instance of this class
*/
CUserInterface * CUserInterface::Instance() 
{
	if (!mInterface)   // Only allow one instance of class to be generated.
		 mInterface = new CUserInterface();
 
   return mInterface;
}

CUserInterface::CUserInterface()
{
	mVertexNormals = mFacesNormals = false;
	mVertexNormals = mFacesNormals = false;
	mBoundingBox = false;
	mStyle[0] = mStyle[2] = false;
	mStyle[1] = true;
	mUserInterface = TwNewBar("Model");
	mLightsControl = TwNewBar("Light");
	shine = 50;
	TwDefine("Model refresh = '0.0001f'");
	TwDefine("Model resizable = false");
	TwDefine("Model fontresizable = false");
	TwDefine("Model movable = false");
	TwDefine("Model position = '20 260'");
	TwDefine("Model size = '250 500'");



	mModelTranslation[0] = 0.0f;
	mModelTranslation[1] = 0.0f;
	mModelTranslation[2] = 0.0f;
	mModelRotation[0] = 0.0f;
	mModelRotation[1] = 0.0f;
	mModelRotation[2] = 0.0f;
	mModelRotation[3] = 1.0f;
	mModelScalar[0] = 1.0f;
	//mModelScalar[1] = 1.0f;
	//mModelScalar[2] = 1.0f;
	mKa[0] = mKa[1] = mKa[2] = 0.5;
	mKd[0] = mKd[1] = mKd[2] = 0.5;
	mKs[0] = mKs[1] = mKs[2] = 0.5;

	TwAddVarRO(mUserInterface, "nameModel", TW_TYPE_STDSTRING, &mNameModel, "label='Model' readonly=true");
	TwAddVarRW(mUserInterface, "X", TW_TYPE_FLOAT, &mModelTranslation[0], " group='Translation' step=0.1 ");
	TwAddVarRW(mUserInterface, "Y", TW_TYPE_FLOAT, &mModelTranslation[1], " group='Translation' step=0.1 ");
	TwAddVarRW(mUserInterface, "Z", TW_TYPE_FLOAT, &mModelTranslation[2], " group='Translation' step=0.1 ");
	TwDefine("Model/Translation group='Transformation'");
	TwAddVarRW(mUserInterface, "Rotation", TW_TYPE_QUAT4F, &mModelRotation, " group='Transformation'");
	TwAddVarRW(mUserInterface, "Xs", TW_TYPE_FLOAT, &mModelScalar[0], " label = 'Scale' group='Scalation' step=0.01 min=0");
	//TwAddVarRW(mUserInterface, "Ys", TW_TYPE_FLOAT, &mModelScalar[1], "  label = 'Y' group='Scalation' step=0.01 min=0");
	//TwAddVarRW(mUserInterface, "Zs", TW_TYPE_FLOAT, &mModelScalar[2], "  label = 'Z' group='Scalation' step=0.01 min=0");
	TwDefine("Model/Scalation group='Transformation'");
	TwAddSeparator(mUserInterface, NULL, NULL);

	TwAddVarRW(mUserInterface, "pointstyle", TW_TYPE_BOOL8, &mStyle[0], "label = 'Points' group='Style' ");
	TwAddVarRW(mUserInterface, "meshstyle", TW_TYPE_BOOL8, &mStyle[1], "label = 'Mesh' group='Style' ");
	TwAddVarRW(mUserInterface, "filledstyle", TW_TYPE_BOOL8, &mStyle[2], "label = 'Filled' group='Style' ");
	TwAddSeparator(mUserInterface, NULL, NULL);
	TwAddVarRW(mUserInterface, "bb", TW_TYPE_BOOL8, &mBoundingBox, "label = 'Bounding Box'");
	TwAddSeparator(mUserInterface, NULL, NULL);
	TwAddVarRW(mUserInterface, "vertexNormals", TW_TYPE_BOOL8, &mVertexNormals, "label = 'Vertex Normals' group='Normals'");
	TwAddVarRW(mUserInterface, "facesNormals", TW_TYPE_BOOL8, &mFacesNormals, "label = 'Faces Normals' group='Normals'");
	TwAddButton(mUserInterface, "invertNormals", invertNormals, NULL, " label='Invert Normals' group='Normals'");
	TwAddSeparator(mUserInterface, NULL, NULL);
	TwAddButton(mUserInterface, " normalsColor", normalsColor, NULL, " label='Normals' group='Color'");
	TwAddButton(mUserInterface, "BbColor", bbColor, NULL, " label='Bounding Box' group='Color'");
	TwAddVarRW(mUserInterface, "ka", TW_TYPE_COLOR3F, &mKa, "label = 'Ambiental' group='Material'");
	TwAddVarRW(mUserInterface, "kd", TW_TYPE_COLOR3F, &mKd, "label = 'Diffuse' group='Material'");
	TwAddVarRW(mUserInterface, "ks", TW_TYPE_COLOR3F, &mKs, "label = 'Specular' group='Material'");
	TwAddVarRW(mUserInterface, "shine", TW_TYPE_FLOAT, &shine, "label = 'Shine' group='Material' step = 0.1 max = 200 min = 0");
	TwDefine("Model/Color group='Material'");
	TwAddSeparator(mUserInterface, NULL, NULL);
	
	TwDefine("Light refresh = '0.0001f'");
	TwDefine("Light resizable = false");
	TwDefine("Light fontresizable = false");
	TwDefine("Light movable = true");
	TwDefine("Light position = '20 260'");
	TwDefine("Light size = '250 300'");
	TwAddSeparator(mLightsControl, NULL, NULL);
	TwAddVarRW(mLightsControl, "Light1X", TW_TYPE_FLOAT, &mLight1Trans[0], " label='Light 1 X' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light1Y", TW_TYPE_FLOAT, &mLight1Trans[1], " label='Light 1 Y' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light1Z", TW_TYPE_FLOAT, &mLight1Trans[2], " label='Light 1 Z' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light1a", TW_TYPE_COLOR3F, &mLight1la[0], "label = 'Light 1 Ambiental' group='Lights'");
	TwAddVarRW(mLightsControl, "Light1d", TW_TYPE_COLOR3F, &mLight1ld[0], "label = 'Light 1 Diffuse' group='Lights'");
	TwAddVarRW(mLightsControl, "Light1s", TW_TYPE_COLOR3F, &mLight1ls[0], "label = 'Light 1 Specular' group='Lights'");
	TwAddSeparator(mLightsControl, NULL, NULL);
	TwAddVarRW(mLightsControl, "Light2X", TW_TYPE_FLOAT, &mLight2Trans[0], " label='Light 2 X' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light2Y", TW_TYPE_FLOAT, &mLight2Trans[1], " label='Light 2 Y' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light2Z", TW_TYPE_FLOAT, &mLight2Trans[2], " label='Light 2 Z' group='Lights' step=0.1 ");
	TwAddVarRW(mLightsControl, "Light2a", TW_TYPE_COLOR3F, &mLight2la[0], "label = 'Light 2 Ambiental' group='Lights'");
	TwAddVarRW(mLightsControl, "Light2d", TW_TYPE_COLOR3F, &mLight2ld[0], "label = 'Light 2 Diffuse' group='Lights'");
	TwAddVarRW(mLightsControl, "Light2s", TW_TYPE_COLOR3F, &mLight2ls[0], "label = 'Light 2 Specular' group='Lights'");
}

CUserInterface::~CUserInterface()
{
}

void CUserInterface::reshape()
{
	TwWindowSize(gWidth, gHeight);
}

void CUserInterface::show()
{
	TwDefine("Model visible = true");
}

void CUserInterface::hide()
{
	TwDefine("Model visible = false");
}

void CUserInterface::setModelTranslation(glm::vec3 modelTranslation)
{
	mModelTranslation[0] = modelTranslation[0];
	mModelTranslation[1] = modelTranslation[1];
	mModelTranslation[2] = modelTranslation[2];
}
void CUserInterface::setLightA(glm::vec3 la, int id)
{
	switch (id)
	{
	case 1:
		mLight1la[0] = la[0];
		mLight1la[1] = la[1];
		mLight1la[2] = la[2];
		break;
	case 2:
		mLight2la[0] = la[0];
		mLight2la[1] = la[1];
		mLight2la[2] = la[2];
	}
}

void CUserInterface::setLightD(glm::vec3 ld, int id)
{
	switch (id)
	{
	case 1:
		mLight1ld[0] = ld[0];
		mLight1ld[1] = ld[1];
		mLight1ld[2] = ld[2];
		break;
	case 2:
		mLight2ld[0] = ld[0];
		mLight2ld[1] = ld[1];
		mLight2ld[2] = ld[2];
	}
}

void CUserInterface::setLightS(glm::vec3 ls, int id)
{
	switch (id)
	{
	case 1:
		mLight1ls[0] = ls[0];
		mLight1ls[1] = ls[1];
		mLight1ls[2] = ls[2];
		break;
	case 2:
		mLight2ls[0] = ls[0];
		mLight2ls[1] = ls[1];
		mLight2ls[2] = ls[2];
	}
}

void CUserInterface::setModelLightPos(glm::vec3 lightTranslation, int id)
{
	switch (id)
	{
	case 1:
		mLight1Trans[0] = lightTranslation[0];
		mLight1Trans[1] = lightTranslation[1];
		mLight1Trans[2] = lightTranslation[2];
		break;
	case 2:
		mLight2Trans[0] = lightTranslation[0];
		mLight2Trans[1] = lightTranslation[1];
		mLight2Trans[2] = lightTranslation[2];
		break;
	}
}

glm::vec3 CUserInterface::getModelLightPos(int id)
{
	switch (id)
	{
	case 1:
		return mLight1Trans;
		break;
	case 2:
		return mLight2Trans;
		break;
	}
}

glm::vec3 CUserInterface::getModelLightA(int id)
{
	switch (id)
	{
	case 1:
		return mLight1la;
		break;
	case 2:
		return mLight2la;
		break;
	}
}

glm::vec3 CUserInterface::getModelLightD(int id)
{
	switch (id)
	{
	case 1:
		return mLight1ld;
		break;
	case 2:
		return mLight2ld;
		break;
	}
}

glm::vec3 CUserInterface::getModelLightS(int id)
{
	switch (id)
	{
	case 1:
		return mLight1ls;
		break;
	case 2:
		return mLight2ls;
		break;
	}
}

glm::vec3 CUserInterface::getModelTranslation()
{
	return mModelTranslation;

}

void CUserInterface::setModelRotation(float * modelRotation)
{
	mModelRotation[0] = modelRotation[0];
	mModelRotation[1] = modelRotation[1];
	mModelRotation[2] = modelRotation[2];
	mModelRotation[3] = modelRotation[3];
}

void CUserInterface::setModelScalar(glm::vec3 scalar)
{
	mModelScalar[0] = scalar[0];
	mModelScalar[1] = scalar[1];
	mModelScalar[2] = scalar[2];
}

void CUserInterface::setNameModel(string name)
{
	mNameModel = name;
}



float * CUserInterface::getModelRotation()
{
	return mModelRotation;
}

glm::vec3 CUserInterface::getModelScalar()
{
	return mModelScalar;
}

void CUserInterface::setVertexNormals(float b){
	mVertexNormals = b;
}

void CUserInterface::setFacesNormals(float b){
	mFacesNormals = b;
}

void CUserInterface::setBondingBox(bool s){
	mBoundingBox = s;
}

void CUserInterface::setStyle(int id, bool b){
	mStyle[id] = b;
}

bool CUserInterface::getBoundingBox(){
	return mBoundingBox;
}

float CUserInterface::getVertexNormals(){
	return mVertexNormals;
}

float CUserInterface::getFacesNormals(){
	return mFacesNormals;
}

bool CUserInterface::getStyle(int id){
	return mStyle[id];
}

void CUserInterface::setModelKA(float* ka)
{
	mKa[0] = ka[0];
	mKa[1] = ka[1];
	mKa[2] = ka[2];
}

void CUserInterface::setModelKD(float* kd)
{
	mKd[0] = kd[0];
	mKd[1] = kd[1];
	mKd[2] = kd[2];
}

void CUserInterface::setModelKS(float* ks)
{
	mKs[0] = ks[0];
	mKs[1] = ks[1];
	mKs[2] = ks[2];
}

void CUserInterface::setModelKA(glm::vec3 ka)
{
	mKa[0] = ka.x;
	mKa[1] = ka.y;
	mKa[2] = ka.z;
}

void CUserInterface::setModelKD(glm::vec3 kd)
{
	mKd[0] = kd.x;
	mKd[1] = kd.y;
	mKd[2] = kd.z;
}

void CUserInterface::setModelKS(glm::vec3 ks)
{
	mKs[0] = ks.x;
	mKs[1] = ks.y;
	mKs[2] = ks.z;
}

float* CUserInterface::getModelKA()
{
	return mKa;
}

float* CUserInterface::getModelKD()
{
	return mKd;
}

float* CUserInterface::getModelKS()
{
	return mKs;
}

float CUserInterface::getShine()
{
	return shine;
}