#pragma once

#include "Model.h"
#include <iostream>
#include <fstream>

using std::fstream;

class CPlayer : public CModel
{
public:
	CPlayer();
	~CPlayer();
	bool load(string path);
};