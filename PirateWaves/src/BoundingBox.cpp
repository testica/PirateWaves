#include "BoundingBox.h"

BoundingBox::BoundingBox(){

	active = true;
	color.x = color.y = color.z = 1.0;
}

BoundingBox:: BoundingBox(Vector3 min, Vector3 max){
	color.x = color.y = color.z = 1.0;
	vertex[0] = min;
	vertex[1] = max;
	vertex[2] = Vector3(min.x, min.y, max.z);
	vertex[3] = Vector3(min.x, max.y, min.z);
	vertex[4] = Vector3(max.x, min.y, min.z);
	vertex[5] = Vector3(min.x, max.y, max.z);
	vertex[6] = Vector3(max.x, min.y, max.z);
	vertex[7] = Vector3(max.x, max.y, min.z);

}
void BoundingBox::display(){
	glColor3f(color.x, color.y, color.z);
	glBegin(GL_LINES);
	glVertex3f(vertex[5].x, vertex[5].y, vertex[5].z);
	glVertex3f(vertex[1].x, vertex[1].y, vertex[1].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[1].x, vertex[1].y, vertex[1].z);
	glVertex3f(vertex[7].x, vertex[7].y, vertex[7].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[7].x, vertex[7].y, vertex[7].z);
	glVertex3f(vertex[3].x, vertex[3].y, vertex[3].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[3].x, vertex[3].y, vertex[3].z);
	glVertex3f(vertex[5].x, vertex[5].y, vertex[5].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[2].x, vertex[2].y, vertex[2].z);
	glVertex3f(vertex[6].x, vertex[6].y, vertex[6].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[6].x, vertex[6].y, vertex[6].z);
	glVertex3f(vertex[4].x, vertex[4].y, vertex[4].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[4].x, vertex[4].y, vertex[4].z);
	glVertex3f(vertex[0].x, vertex[0].y, vertex[0].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[0].x, vertex[0].y, vertex[0].z);
	glVertex3f(vertex[2].x, vertex[2].y, vertex[2].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[5].x, vertex[5].y, vertex[5].z);
	glVertex3f(vertex[2].x, vertex[2].y, vertex[2].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[1].x, vertex[1].y, vertex[1].z);
	glVertex3f(vertex[6].x, vertex[6].y, vertex[6].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[7].x, vertex[7].y, vertex[7].z);
	glVertex3f(vertex[4].x, vertex[4].y, vertex[4].z);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(vertex[3].x, vertex[3].y, vertex[3].z);
	glVertex3f(vertex[0].x, vertex[0].y, vertex[0].z);
	glEnd();


}