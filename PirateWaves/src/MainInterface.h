#pragma once
#include "AntTweakBar.h"
#include <iostream>
#include <string>

using std::string;

//Singleton user interface class
enum DisplayMode
{
	glBeginEnd,
	DisplayList,
	VertexBufferObject,
	VertexPointer
};
class CMainInterface
{
private:
	static CMainInterface * mMain; //Holds the instance of the class
	TwBar *mMainInterface;
	int mModel;
	bool  mMatrixMode, mBFC, mZB;


public:
	///Method to obtain the only instance of the calls
	static CMainInterface * Instance();
	~CMainInterface();
	void reshape();
	void show();
	void hide();
	void setMaxStep(int max);
	int getModelSelected();
	void setBackFaceCull(bool b);
	void setZBuff(bool b);
	void setMatrixMode(bool b);
	bool getBackFaceCull();
	bool getZBuff();
	bool getMatrixMode();
	

private:
	///Private constructor
	CMainInterface();
};