#include "Obj.h"

CObj::CObj()
{
}

CObj::~CObj()
{
}

bool CObj::load(string path)
{
	fstream file;
	string token;
	size_t found = path.find_last_of("/\\");
	nameModel = path.substr(found+1);

	file.open(path, std::ios::in);
	if (file.fail())
	{
		std::cout << "Error: could not open file <"
			<< path << ">" << std::endl;
		return false;
	}

	string line;
	Vector2dVector faces;
	Vector2dVector result;
	Vector3 max, min;
	max.x = max.y = max.z = std::numeric_limits<int>::min();
	min.x = min.y = min.z = std::numeric_limits<int>::max();
	float x, y, z, maxDim;
	int offset;
	char *cstr;
	int A1, A2, A3, B1, B2, B3, C1, C2, C3, D1, D2;
	char prefix[3];
	bool one = false;
	int hashIndex = 0;
	while (!file.eof()){
		getline(file, line);
		int NumRead = sscanf(line.c_str(), "%2s %f %f %f", prefix, &x, &y, &z);
		if (NumRead < 1) continue;
		switch (prefix[0])
		{
			case '#':
				continue;

			case 'v':
			{
				switch (prefix[1])
				{
				case 0:
					if (NumRead != 4) continue;
					mVertices.push_back(Vector3(x, y, z));

					max.x = MAX(max.x, x);
					max.y = MAX(max.y, y);
					max.z = MAX(max.z, z);

					min.x = MIN(min.x, x);
					min.y = MIN(min.y, y);
					min.z = MIN(min.z, z);

					break;
				case 't':
					continue;
					break;
				case 'n':
					continue;
					break;
				}

			}
			break;

			case 'f':
			{
				cstr = new char[line.length() + 1];
				char * aux = cstr;
				strcpy(cstr, line.c_str());
				if (sscanf(cstr, "%2s %d/%d %d/%d %d/%d %n", prefix, &A1, &A2, &B1, &B2, &C1, &C2 ,&offset) == 7)
				{
					
					faces.push_back(mVertices[A1 - 1]);
					faces.push_back(mVertices[B1 - 1]);
					faces.push_back(mVertices[C1 - 1]);
					cstr += offset;
					while (sscanf(cstr, " %d/%d/%d%n", &A1, &A2, &A3, &offset) > 0){
						faces.push_back(mVertices[A1 - 1]);
						cstr += offset;
					}
					if (faces.size() != 3){
						for (int k = 1; k < faces.size() - 1; ++k)
						{
							result.push_back(faces[0]);
							result.push_back(faces[k]);
							result.push_back(faces[k + 1]);

						}
						triangulate(result);
					}
					else{
						triangulate(faces);
					}
					
				}
				else if (sscanf(cstr, "%2s %d/%d/%d %d/%d/%d %d/%d/%d%n", prefix, &A1, &A2, &A3, &B1, &B2, &B3, &C1, &C2, &C3, &offset) == 10)
				{
					
					faces.push_back(mVertices[A1 - 1]);
					faces.push_back(mVertices[B1 - 1]);
					faces.push_back(mVertices[C1 - 1]);
					cstr += offset;
					while (sscanf(cstr, " %d/%d/%d%n", &A1, &A2, &A3, &offset) > 0){
						faces.push_back(mVertices[A1 - 1]);
						cstr += offset;
					}
					if (faces.size() != 3){
						for (int k = 1; k < faces.size() - 1; ++k)
						{
							result.push_back(faces[0]);
							result.push_back(faces[k]);
							result.push_back(faces[k + 1]);

						}
						triangulate(result);
					}
					else{
						triangulate(faces);
					}
				}
				else if (sscanf(cstr, "%2s %d//%d %d//%d %d//%d%n", prefix, &A1, &A3, &B1, &B3, &C1, &C3, &offset) == 7)
				{
					
					faces.push_back(mVertices[A1 - 1]);
					faces.push_back(mVertices[B1 - 1]);
					faces.push_back(mVertices[C1 - 1]);
					cstr += offset;
					
					while (sscanf(cstr, " %d//%d%n", &A1, &A2, &offset)>0){
						
						faces.push_back(mVertices[A1 - 1]);
						cstr += offset;
					}
					if (faces.size() != 3){
						for (int k = 1; k < faces.size() - 1; ++k)
						{
							result.push_back(faces[0]);
							result.push_back(faces[k]);
							result.push_back(faces[k + 1]);

						}
						triangulate(result);
					}
					else{
						triangulate(faces);
					}
					
				}
				else if (sscanf(cstr, "%2s %d %d %d%n", prefix, &A1, &B1, &C1, &offset) == 4)
				{
					
					faces.push_back(mVertices[A1 - 1]);
					faces.push_back(mVertices[B1 - 1]);
					faces.push_back(mVertices[C1 - 1]);
					cstr += offset;
					while (sscanf(cstr, " %d%n", &A1, &offset) > 0){
						
						faces.push_back(mVertices[A1 - 1]);
						cstr += offset;
					}
					if (faces.size() != 3){
						for (int k = 1; k < faces.size() - 1; ++k)
						{
							result.push_back(faces[0]);
							result.push_back(faces[k]);
							result.push_back(faces[k + 1]);

						}
						triangulate(result);
					}
					else{
						triangulate(faces);
					}
					
				}
				faces.clear();
				result.clear();
				delete[] aux;
			}
			break;
		}

	}

	
	file.close();
	mVertices.clear();
	calculateNormals();
	center = (max + min)/2;
	maxDim = MAX(MAX(max.x, max.y), max.z);
	bb = BoundingBox(min/maxDim, max/maxDim);
	center = (max/maxDim + min/maxDim) / 2;
	for (int i = 0; i < mNormals.size(); i++){

		for (int j = 0; j < mVertexNormals.size(); j++){
			if (mNormals[i].vertex == mVertexNormals[j].vertex){
				mNormals[i].normal = mVertexNormals[j].normal;
				j = mVertexNormals.size();
			}
		}

	}
	for (int i = 0; i < mTriangles.size(); i++)
	{
		mTriangles[i].a = mTriangles[i].a / maxDim;
		mTriangles[i].b = mTriangles[i].b / maxDim;
		mTriangles[i].c = mTriangles[i].c / maxDim;
	}
	for (int i = 0; i < mVertexNormals.size(); i++)
	{
		mVertexNormals[i].normal = mVertexNormals[i].normal / maxDim;
		mVertexNormals[i].vertex = mVertexNormals[i].vertex / maxDim;
	}

	for (int i = 0; i < mFacesNormals.size(); i++)
	{
		mFacesNormals[i].normal = mFacesNormals[i].normal / maxDim;
		mFacesNormals[i].vertex = mFacesNormals[i].vertex / maxDim;
	}
	mNumOfVertices = (mTriangles.size()) * 9;
	mNumOfNormals = (mNormals.size()*3);
	float * vertices = new float[mNumOfVertices];
	float * normals = new float[mNumOfNormals];
	/* COPIA DE LOS VERTICES EN UN ARREGLO DE FLOTANTES*/
	for (int i = 0; i < mTriangles.size(); i++){

		vertices[i * 9 + 0] = mTriangles[i].a.x;
		vertices[i * 9 + 1] = mTriangles[i].a.y;
		vertices[i * 9 + 2] = mTriangles[i].a.z;
		vertices[i * 9 + 3] = mTriangles[i].b.x;
		vertices[i * 9 + 4] = mTriangles[i].b.y;
		vertices[i * 9 + 5] = mTriangles[i].b.z;
		vertices[i * 9 + 6] = mTriangles[i].c.x;
		vertices[i * 9 + 7] = mTriangles[i].c.y;
		vertices[i * 9 + 8] = mTriangles[i].c.z;

	}
	
	for (int i = 0; i <mNormals.size(); i++){

		normals[i * 3 + 0] = mNormals[i].normal.x;
		normals[i * 3 + 1] = mNormals[i].normal.y;
		normals[i * 3 + 2] = mNormals[i].normal.z;
	}

	
	GLuint vbo[2]; 
	glGenBuffers(2, vbo); /* asignar cantidad de vbo's*/
	glGenVertexArrays(1, &mVao); /*asignar cantidad de vao's*/
	glBindVertexArray(mVao); /*Enlazar vao y hacer uso de el*/

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, mNumOfVertices * sizeof(float), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL); /* index 0 y contiene 3 flotantes por vertice */
	glEnableVertexAttribArray(0); /*habilita el index 0*/
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, mNumOfNormals * sizeof(float), normals, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL); /* index 1 y contiene 3 flotantes por vertice */
	glEnableVertexAttribArray(1); /*habilita el index 1*/

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	delete[] vertices;
	delete[] normals;
	
	
	return true;
		
}