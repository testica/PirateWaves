#pragma once
#include "glfw3.h"
#include "Vector3.h"

class BoundingBox{

private:
	Vector3 min, max;
	Vector3 vertex[8];

public:
	float active;
	Vector3 color;
	BoundingBox();
	BoundingBox(Vector3 min, Vector3 max);
	void display();

};