#include "Model.h"

CModel::CModel()
{
	mTranslation.x = mTranslation.y = mTranslation.z = 0.0f;
	ka[0] = ka[1] = ka[1] = ka[2] = 0.5f;
	kd[0] = kd[1] = kd[1] = kd[2] = 0.5f;
	ks[0] = ks[1] = ks[1] = ks[2] = 0.5f;

	bb = BoundingBox();
	vertexNormals = facesNormals = false;
	mScalar.x= mScalar.y = mScalar.z = 1.0f;
	normalsColor.x = 1.0;
	normalsColor.y = normalsColor.z = 0.0f;
	
	mRotation[0] = mRotation[1] = mRotation[2] = 0.0f;
	mRotation[3] = 1.0f;
	mStyle[0] = mStyle[2] = false;
	mStyle[1] = true;

	mNumOfNormals = mNumOfVertices = mNumOfTextures= 0;
}

CModel::~CModel()
{
	mVertices.clear();
	mTriangles.clear();
	mFacesNormals.clear();
	mVertexNormals.clear();
	mNormals.clear();
	if (glIsVertexArray(mVao))
		glDeleteVertexArrays(1, &mVao);
}

void CModel::display()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glBindVertexArray(mVao);
		glDrawArrays(GL_TRIANGLES, 0, mNumOfVertices);
	glBindVertexArray(0);
}



void CModel::triangulate(Vector2dVector result){
	int tcount = result.size() / 3;
	bool found[3];
	for (int j = 0; j<tcount; j++)
	{
		found[0] = found[1] = found[2] = false;
		Triangle triangle;
		triangle.a = result[j * 3 + 0];
		triangle.b = result[j * 3 + 1];
		triangle.c = result[j * 3 + 2];
		mTriangles.push_back(triangle);

		Vector3 u, v, n;
		/* Get two vectors u = b-a & v =c-a  */
		u = triangle.b - triangle.a;
		v = triangle.c - triangle.a;
		n = (u.CrossProduct(v));

		/*add normals of face*/
		Normal normal;
		normal.vertex = Vector3((triangle.a.x + triangle.b.x + triangle.c.x) / 3, (triangle.a.y + triangle.b.y + triangle.c.y) / 3, (triangle.a.z + triangle.b.z + triangle.c.z) / 3);
		normal.normal = n;
		mFacesNormals.push_back(normal);

		/*add normals of vertex*/
		for (int i = 0; i < mVertexNormals.size(); i++){

			if (found[0] && found[1] && found[2]) break;
			if (!found[0] && triangle.a == mVertexNormals[i].vertex){
				/* check shared normals and sum */
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[0] = true;
			}
			else if (!found[1] && triangle.b == mVertexNormals[i].vertex){
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[1] = true;
			}
			else if (!found[2] && triangle.c == mVertexNormals[i].vertex){
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[2] = true;
			}
		}

		if (!found[0]){

			normal.vertex = Vector3(triangle.a.x, triangle.a.y, triangle.a.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}

		if (!found[1]){

			normal.vertex = Vector3(triangle.b.x, triangle.b.y, triangle.b.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}

		if (!found[2]){

			normal.vertex = Vector3(triangle.c.x, triangle.c.y, triangle.c.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}
			
		normal.vertex = Vector3(triangle.a.x, triangle.a.y, triangle.a.z);
		normal.normal = (n);
		mNormals.push_back(normal);
		normal.vertex = Vector3(triangle.b.x, triangle.b.y, triangle.b.z);
		normal.normal = (n);
		mNormals.push_back(normal);
		normal.vertex = Vector3(triangle.c.x, triangle.c.y, triangle.c.z);
		normal.normal = (n);
		mNormals.push_back(normal);
			

	}
}

void CModel::calculateNormals(){

	for (int i = 0; i < mVertexNormals.size(); i++)
	{

		mVertexNormals[i].normal = mVertexNormals[i].normal.Normalize();
	}

	for (int i = 0; i < mFacesNormals.size(); i++)
	{

		mFacesNormals[i].normal = mFacesNormals[i].normal.Normalize();
	}

	for (int i = 0; i < mNormals.size(); i++)
	{

		mNormals[i].normal = mNormals[i].normal.Normalize();
	}

}


void CModel::setTranslation(glm::vec3 translation)
{
	mTranslation.x = translation[0];
	mTranslation.y = translation[1];
	mTranslation.z = translation[2];
}

Vector3 CModel::getTranslation()
{
	return mTranslation;
}

void CModel::setScalar(glm::vec3 scalar)
{
	mScalar.x = scalar[0];
	mScalar.y = scalar[1];
	mScalar.z = scalar[2];
}

void CModel::setRotation(float *  rotation)
{
	mRotation[0] = rotation[0];
	mRotation[1] = rotation[1];
	mRotation[2] = rotation[2];
	mRotation[3] = rotation[3];
}


string CModel::getNameModel()
{
	return nameModel;
}

float* CModel::getRotation()
{
	return mRotation;
}

Vector3 CModel::getScalar()
{
	return mScalar;
}


void CModel::calculateVertexNormals(){
	bool found[3];
	Vector3 n;
	Normal normal;
	for (int j = 0; j < mTriangles.size(); j++){
		found[0] = found[1] = found[2] = false;
		Vector3 u, v;
		/* Get two vectors u = b-a & v =c-a  */
		u = mTriangles[j].b - mTriangles[j].a;
		v = mTriangles[j].c - mTriangles[j].a;
		n = (u.CrossProduct(v));
		for (int i = 0; i < mVertexNormals.size(); i++){
			if (mTriangles[j].a == mVertexNormals[i].vertex){
				/* check shared normals and sum */
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[0] = true;
			}
			else if (mTriangles[j].b == mVertexNormals[i].vertex){
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[1] = true;
			}
			else if (mTriangles[j].c == mVertexNormals[i].vertex){
				mVertexNormals[i].normal = mVertexNormals[i].normal + (n);
				found[2] = true;
			}
		}

		if (!found[0]){

			normal.vertex = Vector3(mTriangles[j].a.x, mTriangles[j].a.y, mTriangles[j].a.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}

		if (!found[1]){

			normal.vertex = Vector3(mTriangles[j].b.x, mTriangles[j].b.y, mTriangles[j].b.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}

		if (!found[2]){

			normal.vertex = Vector3(mTriangles[j].c.x, mTriangles[j].c.y, mTriangles[j].c.z);
			normal.normal = (n);
			mVertexNormals.push_back(normal);
		}



	}
	for (int i = 0; i < mVertexNormals.size(); i++)
	{
		/* Normalize all normals*/
		mVertexNormals[i].normal = mVertexNormals[i].normal.Normalize();
	}


}

void CModel::calculateFacesNormals(){

	Vector3 n;
	Normal normal;
	for (int j = 0; j < mTriangles.size(); j++){
		Vector3 u, v;
		/* Get two vectors u = b-a & v =c-a  */
		u = mTriangles[j].b - mTriangles[j].a;
		v = mTriangles[j].c - mTriangles[j].a;
		n = (u.CrossProduct(v));
		normal.vertex = Vector3((mTriangles[j].a.x + mTriangles[j].b.x + mTriangles[j].c.x) / 3, (mTriangles[j].a.y + mTriangles[j].b.y + mTriangles[j].c.y) / 3, (mTriangles[j].a.z + mTriangles[j].b.z + mTriangles[j].c.z) / 3);
		normal.normal = n;
		mFacesNormals.push_back(normal);
	}

	for (int i = 0; i < mFacesNormals.size(); i++)
	{
		/* Normalize all normals*/
		mFacesNormals[i].normal = mFacesNormals[i].normal.Normalize();
	}

}

void CModel::setFacesNormals(bool b){
	facesNormals = b;
	
}

void CModel::setVertexNormals(bool b){
	vertexNormals = b;
	
}

void CModel::invert(){

	for (int i = 0; i < mVertexNormals.size(); i++)
	{
		/* Normalize all normals*/
		mVertexNormals[i].normal = mVertexNormals[i].normal.Invert();
	}

	for (int i = 0; i < mFacesNormals.size(); i++)
	{

		mFacesNormals[i].normal = mFacesNormals[i].normal.Invert();
	}
	

}


void CModel::setColor(glm::vec3 a, glm::vec3 d, glm::vec3 s){
	ka[0] = a[0];
	ka[1] = a[1];
	ka[2] = a[2];
	kd[0] = d[0];
	kd[1] = d[1];
	kd[2] = d[2];
	ks[0] = s[0];
	ks[1] = s[1];
	ks[2] = s[2];
}
void CModel::setNormalsColor(Vector3 c){
	normalsColor = c;
	
}

void CModel::setBoundingBox(bool s){
	bb.active = s;
	
}

void CModel::setStyle(int id, bool b){
	if (id >= 0 && id < 3)
		mStyle[id] = b;
	
}

void CModel::setBbColor(Vector3 c){
	bb.color = c;
	
}

void CModel::setKA(float* c)
{
	ka.x = c[0];
	ka.y = c[1];
	ka.z = c[2];
}

void CModel::setKD(float* c)
{
	kd.x = c[0];
	kd.y = c[1];
	kd.z = c[2];
}

void CModel::setKS(float* c)
{
	ks.x = c[0];
	ks.y = c[1];
	ks.z = c[2];
}

glm::vec3 *  CModel::getColor(){
	glm::vec3 * color = new glm::vec3[3];

	color[0][0] = ka[0];
	color[0][1] = ka[1];
	color[0][2] = ka[2];
	color[1][0] = kd[0];
	color[1][1] = kd[1];
	color[1][2] = kd[2];
	color[2][0] = ks[0];
	color[2][1] = ks[1];
	color[2][2] = ks[2];

	return color;
}

glm::vec3 CModel::getKA()
{
	return ka;
}

glm::vec3 CModel::getKD()
{
	return kd;
}
glm::vec3 CModel::getKS()
{
	return ks;
}

Vector3 CModel::getNormalsColor(){
	return normalsColor;
}

Vector3 CModel::getBbColor(){
	return bb.color;
}

bool CModel::getBoundingBox(){
	return bb.active;
}

bool  CModel::getStyle(int id){
	return mStyle[id];
}

Vector3 CModel::getCenter(){
	return center;
}

bool CModel::getVertexNormals(){
	return vertexNormals;
}

bool CModel::getFacesNormals(){
	return facesNormals;
}



void CModel::displayNormals(char n)
{
	switch (n)
	{
	case 'v':
		if (vertexNormals){
			for (int i = 0; i < mVertexNormals.size(); i++)
			{
				glLineWidth(2.0);
				glColor3f(normalsColor.x, normalsColor.y, normalsColor.z);
				glBegin(GL_LINES);
				glVertex3f((mVertexNormals[i].normal.x) + mVertexNormals[i].vertex.x, (mVertexNormals[i].normal.y) + mVertexNormals[i].vertex.y, (mVertexNormals[i].normal.z) + mVertexNormals[i].vertex.z);
				glVertex3f(mVertexNormals[i].vertex.x, mVertexNormals[i].vertex.y, mVertexNormals[i].vertex.z);
				glEnd();
			}
		}
		break;
	case 'f':
		if (facesNormals){
			for (int i = 0; i < mFacesNormals.size(); i++)
			{
				glLineWidth(2.0);
				glColor3f(normalsColor.x, normalsColor.y, normalsColor.z);
				glBegin(GL_LINES);
				glVertex3f((mFacesNormals[i].normal.x) + mFacesNormals[i].vertex.x, (mFacesNormals[i].normal.y) + mFacesNormals[i].vertex.y, (mFacesNormals[i].normal.z) + mFacesNormals[i].vertex.z);
				glVertex3f(mFacesNormals[i].vertex.x, mFacesNormals[i].vertex.y, mFacesNormals[i].vertex.z);
				glEnd();
			}
		}
	}
}
