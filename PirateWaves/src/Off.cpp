#include "Off.h"
#include <iostream>
COff::COff()
{
}

COff::~COff()
{
}

bool COff::load(string path)
{
	fstream file;
	string token;
	Vector3 max, min;
	max.x = max.y = max.z = std::numeric_limits<int>::min();
	min.x = min.y = min.z = std::numeric_limits<int>::max();
	float x, y, z, maxDim;
	int mNumOfFaces, v0, v1, v2, nVertex;
	Vector2dVector faces, result;
	size_t found = path.find_last_of("/\\");
	nameModel = path.substr(found + 1);

	file.open(path, std::ios::in);
	file >> token;
	if (file.fail())
	{
		std::cout << "Error could not open file " << path << std::endl;
	}
	if (token != "OFF")
	{
		file.close();
		return false;
	}
	else
	{
		file >> token;
		mNumOfVertices = atoi(token.c_str());
		file >> token;
		mNumOfFaces = atoi(token.c_str());
		file >> token;
		for (int i = 0; i < mNumOfVertices; i++)
		{
			file >> x >> y >> z;
			mVertices.push_back(Vector3(x, y, z));
			max.x = MAX(max.x, x);
			max.y = MAX(max.y, y);
			max.z = MAX(max.z, z);

			min.x = MIN(min.x, x);
			min.y = MIN(min.y, y);
			min.z = MIN(min.z, z);
		}
		mCenter = (max + min);
		mCenter.x = mCenter.x / 2;
		mCenter.y = mCenter.y / 2;
		mCenter.z = mCenter.z / 2;
		maxDim = MAX(MAX(max.x, max.y), max.z);
		for (int i = 0; i < mNumOfVertices; i++)
			mVertices[i] = mVertices[i]/maxDim;
		for (int i = 0; i < mNumOfFaces; i++)
		{
			file >> nVertex;
			
			for (int i = 0; i < nVertex; i++)
			{
				file >> v0;
				faces.push_back(mVertices[v0]);
				
			}
			
			if (nVertex != 3){
				for (int k = 1; k < faces.size()-1; ++k)
				{
					result.push_back(faces[0]);
					result.push_back(faces[k]);
					result.push_back(faces[k + 1]);
					
				}
				
				triangulate(result);
			}
			else{
				triangulate(faces);
			}
			faces.clear();
			result.clear();
		}
	}
	file.close();
	mVertices.clear();
	calculateNormals();
	//bb = BoundingBox(min / maxDim, max / maxDim);
	center = (max / maxDim + min / maxDim) / 2;
	for (int i = 0; i < mNormals.size(); i++){

		for (int j = 0; j < mVertexNormals.size(); j++){
			if (mNormals[i].vertex == mVertexNormals[j].vertex){
				mNormals[i].normal = mVertexNormals[j].normal;
				j = mVertexNormals.size();
			}
		}

	}

	mNumOfVertices = (mTriangles.size()) * 9;
	mNumOfNormals = (mNormals.size() * 3);
	float * vertices = new float[mNumOfVertices];
	float * normals = new float[mNumOfNormals];
	/* COPIA DE LOS VERTICES EN UN ARREGLO DE FLOTANTES*/
	for (int i = 0; i < mTriangles.size(); i++){

		vertices[i * 9 + 0] = mTriangles[i].a.x;
		vertices[i * 9 + 1] = mTriangles[i].a.y;
		vertices[i * 9 + 2] = mTriangles[i].a.z;
		vertices[i * 9 + 3] = mTriangles[i].b.x;
		vertices[i * 9 + 4] = mTriangles[i].b.y;
		vertices[i * 9 + 5] = mTriangles[i].b.z;
		vertices[i * 9 + 6] = mTriangles[i].c.x;
		vertices[i * 9 + 7] = mTriangles[i].c.y;
		vertices[i * 9 + 8] = mTriangles[i].c.z;

	}

	for (int i = 0; i <mNormals.size(); i++){

		normals[i * 3 + 0] = mNormals[i].normal.x;
		normals[i * 3 + 1] = mNormals[i].normal.y;
		normals[i * 3 + 2] = mNormals[i].normal.z;
	}


	GLuint vbo[2];
	glGenBuffers(2, vbo); /* asignar cantidad de vbo's*/
	glGenVertexArrays(1, &mVao); /*asignar cantidad de vao's*/
	glBindVertexArray(mVao); /*Enlazar vao y hacer uso de el*/

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, mNumOfVertices * sizeof(float), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL); /* index 0 y contiene 3 flotantes por vertice */
	glEnableVertexAttribArray(0); /*habilita el index 0*/

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, mNumOfNormals * sizeof(float), normals, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL); /* index 1 y contiene 3 flotantes por vertice */
	glEnableVertexAttribArray(1); /*habilita el index 1*/

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	delete[] vertices;
	delete[] normals;
	return true;

}