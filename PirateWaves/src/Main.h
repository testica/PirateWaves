#define GLFW_INCLUDE_GLU
#include "glew.h"
#include "glfw3.h"
#include "AntTweakBar.h"
#include <vector>
#include "irrKlang/irrKlang.h"



void TW_CALL loadModel(void * client);
void TW_CALL selectModels(void * client);
void TW_CALL invertNormals(void * client);
void TW_CALL normalsColor(void * client);
void TW_CALL bbColor(void * client);
void updateUserInterface();

void display();

void reshape(GLFWwindow *window, int width, int height);

void keyInput(GLFWwindow *window, int key, int scancode, int action, int mods);

void mouseButton(GLFWwindow* window, int button, int action, int mods);

void cursorPos(GLFWwindow* window, double x, double y);

void charInput(GLFWwindow* window, unsigned int scanChar);

void destroy();

bool initGlfw();

bool initGlew();

bool initUserInterface();

bool initScene();

bool backgroundSound();

int main(void);