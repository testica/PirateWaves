#pragma once
#include <cstring>
#include <cstdlib>
#include <windows.h>
#include <tchar.h>
#include <Commdlg.h>

struct Color{
	float r, g, b;
};


Color displayColor();
void displayOpenFile(char *filename, int sz);