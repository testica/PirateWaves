#pragma once

#define GLFW_INCLUDE_GLU
#include "glew.h"
#include "glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform2.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <string>
#include <stdlib.h>
#include <vector>
#include "Vector3.h"
#include "BoundingBox.h"

using std::string;
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


typedef std::vector< Vector3 > Vector2dVector;

struct Triangle{

	Vector3 a;
	Vector3 b;
	Vector3 c;
};

struct Normal{
	Vector3 normal;
	Vector3 vertex;
	int cant;
};


class CModel
{
	protected:
		GLuint mVao;
		GLuint width, height;
		int mNumOfVertices, mNumOfNormals, mNumOfTextures;
		string nameModel;
		bool mStyle[3], dlFlag, hiperparche;
		Vector3 center;
		Vector3 mTranslation, mCenter, mMax, mMin, mScalar;
		float facesNormals, vertexNormals, mRotation[4], comp[3];
		glm::vec3 ka, kd, ks;
		Vector3 vertexColor;
		Vector3 normalsColor;
		Vector3 bbColor;
		std::vector <Vector3> mVertices;
		std::vector <Vector3> mTexture;
		std::vector <Normal> mFacesNormals;
		std::vector <Normal> mVertexNormals;
		std::vector <Triangle> mTriangles;
		std::vector <Normal> mNormals;

	public:
		BoundingBox bb;
		GLuint gl_texID;
		CModel();
		~CModel();
		virtual bool load(string path) = 0;
		virtual void display();
		void setTranslation(glm::vec3 translation);
		Vector3 getTranslation();

		void triangulate(Vector2dVector result);
		void calculateNormals();
		void setRotation(float * rotation);
		void setScalar(glm::vec3 scalar);
		void calculateVertexNormals();
		void calculateFacesNormals();
		void setFacesNormals(bool b);
		void setVertexNormals(bool b);

		void invert();
		void setColor(glm::vec3 a, glm::vec3 d, glm::vec3 s);
		void setNormalsColor(Vector3 c);
		void setBbColor(Vector3 c);
		void setBoundingBox(bool s);
		void setStyle(int id, bool b);
		void setKA(float* ka);
		void setKD(float* kd);
		void setKS(float* ks);
		void displayNormals(char n);
		glm::vec3 * getColor();
		Vector3 getNormalsColor();
		Vector3 getBbColor();
		string getNameModel();
		Vector3 getCenter();
		bool getStyle(int id);
		bool getBoundingBox();
		float * getRotation();
		glm::vec3 getKA();
		glm::vec3 getKD();
		glm::vec3 getKS();
		Vector3 getScalar();
		bool getVertexNormals();
		bool getFacesNormals();
};