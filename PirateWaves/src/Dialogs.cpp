#include "Dialogs.h"


Color displayColor()
{
	Color c;
	c.r = c.g = c.b = -1.0;

	CHOOSECOLOR info; COLORREF colors[16];
	memset(&info, 0, sizeof(info));

	memset(colors, 0, sizeof(colors));   // Select all colors to black

	info.lStructSize = sizeof(info);
	info.lpCustColors = colors;

	if (ChooseColor(&info) != TRUE)
		return c;

	c.r = GetRValue(info.rgbResult) / 255.0;
	c.g = GetGValue(info.rgbResult) / 255.0;
	c.b = GetBValue(info.rgbResult) / 255.0;

	return c;
}


void displayOpenFile(char *filename, int sz)
{
	OPENFILENAME ofn;       // common dialog box structure
	wchar_t szFile[260];       // buffer for file name
	HWND hwnd = GetForegroundWindow();              // owner window
	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	
	ofn.lpstrFile = szFile;
	// Set lpstrFile[0] to '\0' so that GetOpenFileName does not
	// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.nFilterIndex = 1;
	ofn.lpstrFilter = (LPCWSTR)L"Modelo OBJ (*.obj)\0*.obj\0Modelo OFF (*.off)\0*.off\0";
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	ofn.lpstrInitialDir = NULL;

	// Display the Open dialog box.
	char DefChar = ' ';
	if (GetOpenFileName(&ofn) == TRUE)
		WideCharToMultiByte(CP_ACP, 0, ofn.lpstrFile, -1, filename, sz, &DefChar, NULL); // Convierto a string
}

