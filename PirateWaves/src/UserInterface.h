#pragma once

#include "AntTweakBar.h"
#include "glm/glm.hpp"
#include <iostream>
#include <string>

using std::string;

//Singleton user interface class

class CUserInterface
{
private:
	static CUserInterface * mInterface; //Holds the instance of the class
	TwBar *mUserInterface, *mLightsControl;
	glm::vec3 mModelTranslation, mModelScalar,  mLight1Trans, mLight2Trans;
	glm::vec3 mLight1la, mLight1ld, mLight1ls;
	glm::vec3 mLight2la, mLight2ld, mLight2ls;
	float mModelRotation[4], mKa[3], mKs[3], mKd[3], shine;
	bool mVertexNormals, mFacesNormals, mBoundingBox, mStyle[3];
	string mNameModel;

public:
	///Method to obtain the only instance of the calls
	static CUserInterface * Instance();
	~CUserInterface();
	void reshape();
	void show();
	void hide();
	void setModelTranslation(glm::vec3 modelTranslation);
	void setModelRotation(float * modelRotation);
	void setModelScalar(glm::vec3 modelScalar);
	void setVertexNormals(float b);
	void setFacesNormals(float b);
	void setBondingBox(bool s);
	void setNameModel(string n);
	void setBackFaceCull(bool b);
	void setZBuff(bool b);
	void setStyle(int id, bool b);
	void setModelLightPos(glm::vec3 lightTranslation, int id);
	void setColor(glm::vec3 a, glm::vec3 d, glm::vec3 s);
	void setLightA(glm::vec3 la, int id);
	void setLightD(glm::vec3 ld, int id);
	void setLightS(glm::vec3 ls, int id);
	void setModelKA(float* ka);
	void setModelKD(float* kd);
	void setModelKS(float* ks);
	void setModelKA(glm::vec3 ka);
	void setModelKD(glm::vec3 kd);
	void setModelKS(glm::vec3 ks);
	bool getStyle(int id);
	bool getBoundingBox();
	float getVertexNormals();
	float getFacesNormals();
	glm::vec3 getModelTranslation();
	float * getModelRotation();
	glm::vec3 getModelScalar();
	glm::vec3 getModelLightPos(int id);
	glm::vec3 getModelLightA(int id);
	glm::vec3 getModelLightD(int id);
	glm::vec3 getModelLightS(int id);
	float* getModelKA();
	float* getModelKD();
	float* getModelKS();
	bool getBackFaceCull();
	bool getZBuff();
	float getShine();

private:
	///Private constructor
	CUserInterface(); 
};