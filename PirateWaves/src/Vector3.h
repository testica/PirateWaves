#ifndef VECTOR3_H

#define VECTOR3_H
#include<math.h>
class Vector3{

public:

	float x;
	float y;
	float z;

	Vector3();
	Vector3(float x, float y, float z);
	Vector3 CrossProduct(Vector3 w);
	Vector3 operator-(const Vector3 v);
	Vector3 operator+(const Vector3 v);
	Vector3 operator/(const float v);
	bool operator==(const Vector3 v);
	Vector3 Invert();
	Vector3 Normalize();
	float * toFloat3();
};
#endif