#include "MainInterface.h"
#include "Main.h"
extern int gWidth, gHeight;

// Global static pointer used to ensure a single instance of the class.
CMainInterface *  CMainInterface::mMain = NULL;

/**
* Creates an instance of the class
*
* @return the instance of this class
*/
CMainInterface *  CMainInterface::Instance()
{
	if (!mMain)   // Only allow one instance of class to be generated.
		mMain = new  CMainInterface();

	return mMain;
}

CMainInterface::CMainInterface()
{
	mBFC = false;
	mZB = mMatrixMode = true;

	mMainInterface = TwNewBar("Main");
	TwDefine("Main text = light ");
	TwDefine("Main fontsize=3");
	TwDefine("Main color='255 0 0' alpha=120");
	TwDefine("Main contained=true ");
	TwDefine("Main refresh = '0.0001f'");
	TwDefine("Main resizable = false");
	TwDefine("Main fontresizable = false");
	TwDefine("Main movable = true");
	TwDefine("Main visible = true");
	TwDefine("Main position = '20 20'");
	TwDefine("Main size = '220 220'");
	TwDefine("TW_HELP visible=false");

	TwAddButton(mMainInterface, "loadModel", loadModel, NULL, " label='Load Model' ");
	TwAddSeparator(mMainInterface, NULL, NULL);
	TwAddVarRW(mMainInterface, "inputModels", TW_TYPE_INT32, &mModel, " label='Models' min=0 max=0 step=1  group='Select Models'");
	TwAddButton(mMainInterface, "selectModels", selectModels, NULL, " label='Select' group='Select Models'");
	TwAddSeparator(mMainInterface, NULL, NULL);
	TwAddVarRW(mMainInterface, "BackFaceCullStatus", TW_TYPE_BOOL8, &mBFC, "label = 'Back-Face Culling'");
	TwAddVarRW(mMainInterface, "ZBufferStatus", TW_TYPE_BOOL8, &mZB, "label = 'Z-Buffer'");


}

CMainInterface::~CMainInterface()
{
}

void  CMainInterface::reshape()
{
	TwWindowSize(gWidth, gHeight);
	TwDefine("Main position = '4000 0'");
}

void  CMainInterface::show()
{
	TwDefine("Main visible = true");
}

void  CMainInterface::hide()
{
	TwDefine("Main visible = false");
}

void  CMainInterface::setMaxStep(int max){
	int min = 1;
	TwSetParam(mMainInterface, "inputModels", "min", TW_PARAM_INT32, 1,&min);
	TwSetParam(mMainInterface, "inputModels", "max", TW_PARAM_INT32, 1, &max);
}

void  CMainInterface::setBackFaceCull(bool b)
{
	mBFC = b;
}

void  CMainInterface::setMatrixMode(bool b)
{
	mMatrixMode = b;
}

void  CMainInterface::setZBuff(bool b)
{
	mZB = b;
}

int CMainInterface::getModelSelected(){
	return mModel;
}

bool CMainInterface::getBackFaceCull()
{
	return mBFC;
}

bool CMainInterface::getMatrixMode()
{
	return mMatrixMode;
}

bool CMainInterface::getZBuff()
{
	return mZB;
}
