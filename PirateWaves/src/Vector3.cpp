#include "Vector3.h"

Vector3:: Vector3(){

}

Vector3::Vector3(float x, float y, float z){
	this->x = x;
	this->y = y;
	this->z = z;
}


//A x B = (a2b3 - a3b2, a3b1 - a1b3, a1b2 - a2b1); 
Vector3 Vector3::CrossProduct(Vector3 w){

	return Vector3((y*w.z) - (z*w.y), (z*w.x) - (x*w.z), (x*w.y) - (y*w.x));

}

Vector3 Vector3::operator-(Vector3 v){

	return Vector3(x - v.x, y - v.y, z - v.z);
}

Vector3 Vector3::operator+(Vector3 v){

	return Vector3(x + v.x, y + v.y, z + v.z);
}

Vector3 Vector3::Normalize(){
	Vector3 r;
	float vu = sqrt((x*x) + (y*y) + (z*z));
	r.x = x / vu;
	r.y = y / vu;
	r.z = z / vu;
	return r;
}

Vector3 Vector3::operator/(const float v){
	return Vector3(x/v, y/v, z/v);
}

bool Vector3::operator==(const Vector3 v){
	if (v.x == x && v.y == y && v.z == z)
		return true;
	else
		return false;
}

Vector3 Vector3::Invert(){

	return Vector3(x * -1, y * -1, z * -1);
}

float * Vector3::toFloat3(){
	float aux[3];

	aux[0] = x;
	aux[1] = y;
	aux[2] = z;

	return aux;
}