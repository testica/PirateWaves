#include "Main.h"
#include "Model.h"
#include "SOff.h"
#include "MainInterface.h"
#include "UserInterface.h"
#include "GLSLProgram.h"
#include "Dialogs.h"
#include "Off.h"
#include "Obj.h"
#include "Player.h"
#include "FreeImage/FreeImage.h"



using std::vector;

GLuint gl_texID;
GLFWwindow *gWindow;
int gWidth, gHeight;
CGLSLProgram * glslProgram, *glslProgram2;
CUserInterface * userInterface;
CMainInterface * mainInterface;
Color color;
vector <CModel *> models;
CModel* lights[2];
int picked;
glm::mat4 projection, view;
irrklang::ISoundEngine* engine;
irrklang::ISound* music;



bool  loadTexture(const char* filename, BYTE ** bits, GLuint *w, GLuint *h)
{
	//image format
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	//pointer to the image, once loaded
	FIBITMAP *dib(0);

	//check the file signature and deduce its format
	fif = FreeImage_GetFileType(filename, 0);
	//if still unknown, try to guess the file format from the file extension
	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(filename);
	//if still unkown, return failure
	if (fif == FIF_UNKNOWN)
		return false;

	//check that the plugin has reading capabilities and load the file
	if (FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, filename);
	//if the image failed to load, return failure
	if (!dib)
		return false;

	//retrieve the image data
	BYTE * arr = FreeImage_GetBits(dib);

	//get the image width and height
	(*w) = FreeImage_GetWidth(dib);
	(*h) = FreeImage_GetHeight(dib);

	*bits = new BYTE[*w * *h * 4];

	for (int i = 0; i< *w * *h; ++i){
		(*bits)[i * 4 + 0] = arr[i * 4 + 2];
		(*bits)[i * 4 + 1] = arr[i * 4 + 1];
		(*bits)[i * 4 + 2] = arr[i * 4 + 0];
		(*bits)[i * 4 + 3] = arr[i * 4 + 3];
	}

	//if this somehow one of these failed (they shouldn't), return failure
	if ((*bits == NULL) || (*w == 0) || (*h == 0))
		return false;

	//Free FreeImage's copy of the data
	FreeImage_Unload(dib);

	//return success
	return true;
}


/* SELECCIONAR MODELOS*/
void TW_CALL selectModels(void * client){
	Vector3 translate, scale;
	float * rotate = new float[4];

	if (mainInterface->getModelSelected() > 0 && (mainInterface->getModelSelected() - 1) != picked){
		picked = mainInterface->getModelSelected() - 1;
		translate = models[picked]->getTranslation();
		scale = models[picked]->getScalar();
		userInterface->show();
		userInterface->setNameModel(models[picked]->getNameModel());
		userInterface->setBondingBox(models[picked]->getBoundingBox());
		userInterface->setStyle(0, models[picked]->getStyle(0));
		userInterface->setStyle(1, models[picked]->getStyle(1));
		userInterface->setStyle(2, models[picked]->getStyle(2));
		userInterface->setFacesNormals(models[picked]->getFacesNormals());
		userInterface->setVertexNormals(models[picked]->getVertexNormals());
		userInterface->setModelTranslation(glm::vec3(translate.x, translate.y, translate.z));
		userInterface->setModelRotation(models[picked]->getRotation());
		userInterface->setModelScalar(glm::vec3(scale.x,scale.y,scale.z));
		userInterface->setModelKA(models[picked]->getKA());
		userInterface->setModelKD(models[picked]->getKD());
		userInterface->setModelKS(models[picked]->getKS());

	}

	delete[] rotate;
	
}

/* CARGAR MODELOS  */
void TW_CALL loadModel(void * client){
	char filename[256];
	filename[0] = 0;
	displayOpenFile(filename, 256);
	if (filename[0] != 0){
		string ext = filename;
		size_t found = ext.find_last_of(".");
		ext = ext.substr(found + 1);
		if (ext == "obj"){
			
			CObj *obj = new CObj();

			if (obj->load(filename)){
				
				models.push_back(obj);
				mainInterface->setMaxStep(models.size());
				
			}
			else{
				delete obj;
			}

		}
		else if (ext == "off"){

			COff *off = new COff();
			if (off->load(filename)){

				models.push_back(off);
				mainInterface->setMaxStep(models.size());
				
			}
			else{
				delete off;
			}

		}
	}
}

/* CAMBIAR COLOR A NORMALES */
void TW_CALL normalsColor(void * client){
	color = displayColor();
	models[picked]->setNormalsColor(Vector3(color.r, color.g, color.b));
}

/*CAMBIAR COLOR BOUNDING BOX*/
void TW_CALL bbColor(void * client){

	color = displayColor();
	models[picked]->setBbColor(Vector3(color.r, color.g, color.b));
}

/* INVERTIR NORMALES*/
void TW_CALL invertNormals(void * client){
	if (picked > -1)
		models[picked]->invert();
}



void updateUserInterface()
{
	if (picked > -1){
		userInterface->show();
		models[picked]->setTranslation(userInterface->getModelTranslation());
		float *ka, *kd, *ks;
		ka = userInterface->getModelKA();
		kd = userInterface->getModelKD();
		ks = userInterface->getModelKS();
		if (models[picked]->getScalar().x != userInterface->getModelScalar().x)
		{
			float scale = userInterface->getModelScalar().x;
			models[picked]->setScalar(glm::vec3(scale, scale, scale));
		}
		if (models[picked]->getRotation()[0] != userInterface->getModelRotation()[0] && models[picked]->getRotation()[1] != userInterface->getModelRotation()[1] && models[picked]->getRotation()[2] != userInterface->getModelRotation()[2] && models[picked]->getRotation()[3] != userInterface->getModelRotation()[3])
		{
			models[picked]->setRotation(userInterface->getModelRotation());
		}
		if (models[picked]->getKA() != glm::vec3(ka[0], ka[1], ka[2]))
		{
			models[picked]->setKA(ka);
		}
		if (models[picked]->getKD() != glm::vec3(kd[0], kd[1], kd[2]))
		{
			models[picked]->setKD(kd);
		}
		if (models[picked]->getKS() != glm::vec3(ks[0], ks[1], ks[2]))
		{
			models[picked]->setKS(ks);
		}
		if (models[picked]->bb.active != userInterface->getBoundingBox())
		{
			models[picked]->bb.active = userInterface->getBoundingBox();
		}
		if (models[picked]->getFacesNormals() != userInterface->getFacesNormals())
		{
			models[picked]->setFacesNormals(userInterface->getFacesNormals());
		}
		if (models[picked]->getVertexNormals() != userInterface->getVertexNormals())
		{
			models[picked]->setVertexNormals(userInterface->getVertexNormals());
		}
	}
	else{
		userInterface->hide();
	}

	Vector3 pos;
	for (int i = 1; i <= 2; i++)
	{
		pos = lights[i-1]->getTranslation();
		if (glm::vec3(pos.x, pos.y, pos.z) != userInterface->getModelLightPos(i))
		{
			lights[i-1]->setTranslation(userInterface->getModelLightPos(i));
		}
		if (lights[i-1]->getColor()[0] != userInterface->getModelLightA(i))
		{
			lights[i-1]->setColor(userInterface->getModelLightA(i), userInterface->getModelLightD(i), userInterface->getModelLightS(i));
		}
		if (lights[i-1]->getColor()[1] != userInterface->getModelLightD(i))
		{
			lights[i-1]->setColor(userInterface->getModelLightA(i), userInterface->getModelLightD(i), userInterface->getModelLightS(i));
		}
		if (lights[i-1]->getColor()[2] != userInterface->getModelLightS(i))
		{
			lights[i-1]->setColor(userInterface->getModelLightA(i), userInterface->getModelLightD(i), userInterface->getModelLightS(i));
		}
	}
	

}

//QUATERNION -> MATRIX
void ConvertQuaternionToMatrix(const float *quat, float *mat)
{
	float yy2 = 2.0f * quat[1] * quat[1];
	float xy2 = 2.0f * quat[0] * quat[1];
	float xz2 = 2.0f * quat[0] * quat[2];
	float yz2 = 2.0f * quat[1] * quat[2];
	float zz2 = 2.0f * quat[2] * quat[2];
	float wz2 = 2.0f * quat[3] * quat[2];
	float wy2 = 2.0f * quat[3] * quat[1];
	float wx2 = 2.0f * quat[3] * quat[0];
	float xx2 = 2.0f * quat[0] * quat[0];
	mat[0 * 4 + 0] = -yy2 - zz2 + 1.0f;
	mat[0 * 4 + 1] = xy2 + wz2;
	mat[0 * 4 + 2] = xz2 - wy2;
	mat[0 * 4 + 3] = 0;
	mat[1 * 4 + 0] = xy2 - wz2;
	mat[1 * 4 + 1] = -xx2 - zz2 + 1.0f;
	mat[1 * 4 + 2] = yz2 + wx2;
	mat[1 * 4 + 3] = 0;
	mat[2 * 4 + 0] = xz2 + wy2;
	mat[2 * 4 + 1] = yz2 - wx2;
	mat[2 * 4 + 2] = -xx2 - yy2 + 1.0f;
	mat[2 * 4 + 3] = 0;
	mat[3 * 4 + 0] = mat[3 * 4 + 1] = mat[3 * 4 + 2] = 0;
	mat[3 * 4 + 3] = 1;
}

void display()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Vector3 translate, scale, center;
	float rotMat[4*4], shine;
	float a = 0;
	//glm::mat4x4 rotate;
	glm::mat4 rotate;
	glm::mat4 model;
	glm::mat4 mv;
	glm::vec3 light1pos, light2pos, l1c1, l1c2, l1c3, l2c1, l2c2, l2c3;
	glm::vec3 ka, kd, ks;
	Vector3 l1, l2;
	l1 = lights[0]->getTranslation();
	l2 = lights[1]->getTranslation();
	light1pos = glm::vec3(l1.x, l1.y, l1.z);
	light2pos = glm::vec3(l2.x, l2.y, l2.z);
	//std::cout << l1.x << " " << l1.y << " " << l1.z << std::endl;
	//std::cout << l2.x << " " << l2.y << " " << l2.z << std::endl;
	l1c1 = lights[0]->getColor()[0];
	l1c2 = lights[0]->getColor()[1];
	l1c3 = lights[0]->getColor()[2];
	l2c1 = lights[1]->getColor()[0];
	l2c2 = lights[1]->getColor()[1];
	l2c3 = lights[1]->getColor()[2];

	//Set Active texture index
	glActiveTexture(GL_TEXTURE0);

	
	for (unsigned int i = 0; i < models.size(); i++)
	{	
		//bind to the new texture ID
		glBindTexture(GL_TEXTURE_2D, models[i]->gl_texID);

		shine = userInterface->getShine();
		center = models[i]->getCenter();
		ConvertQuaternionToMatrix(models[i]->getRotation(), rotMat);
		translate = models[i]->getTranslation();
		scale = models[i]->getScalar();
		rotate = glm::make_mat4x4(rotMat);
		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(translate.x, translate.y, translate.z));
		model = glm::translate(model, glm::vec3(center.x, center.y, center.z));
		model = model * rotate;
		model = glm::scale(model, glm::vec3(scale.x, scale.x, scale.x));
		model = glm::translate(model, glm::vec3(-center.x, -center.y, -center.z));
		mv = view * model;

		ka = models[i]->getKA();
		kd = models[i]->getKD();
		ks = models[i]->getKS();
		glm::mat4 mvp = projection * view * model;
		glm::mat4 normalMatrix = glm::transpose(glm::inverse(model));
		glslProgram->enable();
			glslProgram->loadUniformMatrix("modelViewMatrix", GShader::UNIFORM_SIZE_4D, glm::value_ptr(mv));
			glslProgram->loadUniformMatrix("projectionMatrix", GShader::UNIFORM_SIZE_4D, glm::value_ptr(projection));
			glslProgram->loadUniformMatrix("normalMatrix", GShader::UNIFORM_SIZE_4D, glm::value_ptr(normalMatrix));
			glslProgram->loadUniformf("l1Pos", GShader::UNIFORM_SIZE_3D, glm::value_ptr(light1pos));
			glslProgram->loadUniformf("l2Pos", GShader::UNIFORM_SIZE_3D, glm::value_ptr(light2pos));
			glslProgram->loadUniformf("l1Amb", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l1c1));
			glslProgram->loadUniformf("l1Diff", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l1c2));
			glslProgram->loadUniformf("l1Spec", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l1c3));
			glslProgram->loadUniformf("l2Amb", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l2c1));
			glslProgram->loadUniformf("l2Diff", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l2c2));
			glslProgram->loadUniformf("l2Spec", GShader::UNIFORM_SIZE_3D, glm::value_ptr(l2c3));
			glslProgram->loadUniformf("ka", GShader::UNIFORM_SIZE_3D, glm::value_ptr(ka));
			glslProgram->loadUniformf("kd", GShader::UNIFORM_SIZE_3D, glm::value_ptr(kd));
			glslProgram->loadUniformf("ks", GShader::UNIFORM_SIZE_3D, glm::value_ptr(ks));
			glslProgram->loadUniformf("shine", GShader::UNIFORM_SIZE_1D, &(shine));
			glslProgram->loadUniformf("image", GShader::UNIFORM_SIZE_1D, &a);

			//glslProgram->loadUniformMatrix("NormalMatrix", GShader::UNIFORM_SIZE_4D, glm::value_ptr(normalMatrix));
			models[i]->display();
		glslProgram->disable();
		
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf(glm::value_ptr(mv));
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(glm::value_ptr(projection));
		if (models[i]->bb.active)
		{
			models[i]->bb.display();
		}
		if (models[i]->getVertexNormals())
		{
			models[i]->displayNormals('v');
		}
		if (models[i]->getFacesNormals())
		{
			models[i]->displayNormals('f');
		}
	}

	for (int i = 0; i < 2; i++)
	{
		translate = lights[i]->getTranslation();
		center = lights[i]->getCenter();
		scale = lights[i]->getScalar();
		model = glm::mat4(1.0);
		//model = glm::translate(model, glm::vec3(center.x, center.y, center.z));
		model = glm::scale(model, glm::vec3(scale.x, scale.x, scale.x));
		//model = glm::translate(model, glm::vec3(-center.x, -center.y, -center.z));
		//mv = view * model;
		model = glm::translate(model, glm::vec3(translate.x, translate.y, translate.z));
		glm::mat4 mvp = projection * view * model;
		glslProgram2->enable();
			glslProgram2->loadUniformMatrix("MVP", GShader::UNIFORM_SIZE_4D, glm::value_ptr(mvp));
			lights[i]->display();
		glslProgram2->disable();
	}
}

void reshape(GLFWwindow *window, int width, int height)
{
	gWidth = width;
	gHeight = height;

	glViewport(0, 0, gWidth, gHeight);

	userInterface->reshape();

	projection = glm::perspective(45.0f, (float)gWidth / (float)gHeight, 1.0f, 1000.0f);
}

void keyInput(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (TwEventKeyGLFW(key, action))
		return;

	if (action == GLFW_PRESS)
	{
		switch (key)
		{
			case GLFW_KEY_ESCAPE:
				glfwSetWindowShouldClose(window, GL_TRUE);
				break;
		}
	}
}

void mouseButton(GLFWwindow* window, int button, int action, int mods)
{
	if (TwEventMouseButtonGLFW(button, action))
		return;
}

void cursorPos(GLFWwindow* window, double x, double y)
{
	if (TwEventMousePosGLFW(int(x), int(y)))
		return;
}

void charInput(GLFWwindow* window, unsigned int scanChar)
{
	if (TwEventCharGLFW(scanChar, GLFW_PRESS))
		return;
}

void destroy()
{
	for (unsigned int i = 0; i < models.size(); i++)
		delete models[i];

	delete userInterface;

	if (music)
		music->drop(); // release music stream.

	engine->drop(); // delete Engine
	TwTerminate();
	glfwDestroyWindow(gWindow);
	glfwTerminate();
}

bool initGlfw()
{
	if (!glfwInit())
		return false;

	gWindow = glfwCreateWindow(gWidth, gHeight, "Tarea 4", NULL, NULL);

	if (!gWindow)
	{
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(gWindow);

	const GLFWvidmode * vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(gWindow, (vidMode->width - gWidth) >> 1, (vidMode->height - gHeight) >> 1);

	glfwSetWindowSizeCallback(gWindow, reshape);
	glfwSetKeyCallback(gWindow, keyInput);
	glfwSetMouseButtonCallback(gWindow, mouseButton);
	glfwSetCursorPosCallback(gWindow, cursorPos);
	glfwSetCharCallback(gWindow, charInput);

	return true;
}

bool initGlew()
{
	if (glewInit() != GLEW_OK)
		return false;
	else
	{
		printf("Vendor: %s \n", glGetString(GL_VENDOR));
		printf("Renderer: %s \n", glGetString(GL_RENDERER));
		printf("OpenGL Version: %s \n", glGetString(GL_VERSION));
		printf("GLSL Version: %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

		printf("\n\nShader: Color \n");

		glslProgram = new CGLSLProgram();
		glslProgram->loadFromFile("../shaders/Color.vert"); //glslProgram->loadShader("../shaders/Color.vert",CGLSLProgram::VERTEX)
		glslProgram->loadFromFile("../shaders/Color.frag"); //glslProgram->loadShader("../shaders/Color.frag", CGLSLProgram::FRAGMENT)
		glslProgram->create(); //glslProgram->create_link()
		glslProgram->loadUniformVariables();
		glslProgram->loadAttributeVariables();

		printf("\n\nShader: Light \n");

		glslProgram2 = new CGLSLProgram();
		glslProgram2->loadFromFile("../shaders/Light.vert");
		glslProgram2->loadFromFile("../shaders/Light.frag");
		glslProgram2->create();
		glslProgram2->loadUniformVariables();
		glslProgram2->loadAttributeVariables();

		return true;
	}
}

bool initUserInterface()
{
	if (!TwInit(TW_OPENGL, NULL))
		return false;

	userInterface = CUserInterface::Instance();
	mainInterface = CMainInterface::Instance();

	return true;
}

bool initScene()
{
	CPlayer* player = new CPlayer();
	BYTE *bits = NULL;
	GLuint width, height;
	
	
	if(!player->load("../models/yoshipirate.obj"))
		return false;

	if (!loadTexture("../models/yoshipirate.png", &bits, &width, &height))
	{
		std::cout << "ERROR LOADING TEXTURE!!!!" << std::endl;
		return false;
	}

	glGenTextures(1, &player->gl_texID);
	//bind to the new texture ID
	glBindTexture(GL_TEXTURE_2D, player->gl_texID);
	//store the texture data for OpenGL use
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bits);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (bits != NULL)
		delete[] bits;
	
	models.push_back(player);
	mainInterface->setMaxStep(models.size());

	return true;
}

bool backgroundSound(){
	engine = irrklang::createIrrKlangDevice();

	if (!engine)
		return false; // error starting up the engine
	const char * filename = "../files/Ummet Ozcan - Raise Your Hands (Original Mix).mp3";
	music = engine->play2D(filename,true, false, true, irrklang::ESM_AUTO_DETECT, true);
	return true;
}

int initLights()
{
	CObj *light1 = new CObj();
	CObj *light2 = new CObj();
	glm::vec3 lightColorA = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 lightColorD = glm::vec3(0.0, 1.0, 0.0);
	glm::vec3 lightColorS = glm::vec3(0.0, 0.0, 1.0);
	if (light1->load("../models/sphere.obj"))
	{
		lights[0] = light1;
		//lights[0]->setTranslation(glm::vec3(3.0, 3.0, 3.0));
		glm::vec3 lightColorA = glm::vec3(0.0, 0.0, 0.0);
		glm::vec3 lightColorD = glm::vec3(0.0, 1.0, 0.0);
		glm::vec3 lightColorS = glm::vec3(0.0, 0.0, 1.0);
		lights[0]->setScalar(glm::vec3(0.2, 0.2, 0.2));
		Vector3 pos = lights[0]->getTranslation();
		lights[0]->setColor(lightColorA, lightColorD, lightColorS);
		userInterface->setModelLightPos(glm::vec3(pos.x, pos.y, pos.z), 1);
		userInterface->setLightA(lightColorA, 1);
		userInterface->setLightD(lightColorD, 1);
		userInterface->setLightS(lightColorS, 1);
	}
	else{
		return false;
	}
	if (light2->load("../models/sphere.obj"))
	{
		lights[1] = light2;

		//lights[1]->setTranslation(glm::vec3(-3.0, -3.0, -3.0));
		glm::vec3 lightColorA = glm::vec3(0.0, 0.0, 0.0);
		glm::vec3 lightColorD = glm::vec3(0.0, 1.0, 1.0);
		glm::vec3 lightColorS = glm::vec3(1.0, 0.0, 1.0);
		lights[1]->setColor(lightColorA, lightColorD, lightColorS);
		lights[1]->setScalar(glm::vec3(0.2, 0.2, 0.2));
		Vector3 pos = lights[1]->getTranslation();
		userInterface->setModelLightPos(glm::vec3(pos.x, pos.y, pos.z), 2);
		userInterface->setLightA(lightColorA, 2);
		userInterface->setLightD(lightColorD, 2);
		userInterface->setLightS(lightColorS, 2);
	}
	else{
		return false;
	}
	return true;
}
int main(void)
{
	gWidth = 1024;
	gHeight = 768;
	picked = -1;

	if (!initGlfw() || !initGlew() || !initUserInterface() || !initLights() || !backgroundSound() || !initScene())
		return EXIT_FAILURE;

	glEnable(GL_DEPTH_TEST);

	reshape(gWindow, gWidth, gHeight);

	view = glm::lookAt(glm::vec3(3.0f, 3.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	

	while (!glfwWindowShouldClose(gWindow))
	{

		if (mainInterface->getBackFaceCull())
		{
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
		}
		else
		{
			glDisable(GL_CULL_FACE);
		}
		if (mainInterface->getZBuff())
		{
			glEnable(GL_DEPTH_TEST);
		}
		else
		{
			glDisable(GL_DEPTH_TEST);
		}

		display();

		TwDraw();

		updateUserInterface();

		glfwSwapBuffers(gWindow);

		glfwPollEvents();
	}

	destroy();

	return EXIT_SUCCESS;
}